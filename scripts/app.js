/**
 * Created by osman on 5.4.2015.
 */

    "use strict"

var APP = angular.module("foodOrdering_frontFront",["ngRoute"]);

APP.config(function($routeProvider){
   $routeProvider
       .when('/login',{
           templateUrl:"views/partials/login.html",
           controller:"UserCtrl"
       })
});